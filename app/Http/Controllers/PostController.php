<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    function index()
    {
        $posts = Post::All();
        return view('post.index', compact('posts'));
    }

    function create()
    {
        return view('post.form');
    }

    function store(Request $request)
    {
        Post::create($request->only('name', 'title'));
        return redirect(route('post.index'));
    }
}
